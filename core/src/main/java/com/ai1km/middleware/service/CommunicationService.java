package com.ai1km.middleware.service;

import com.ai1km.middleware.domain.EBikeStatesDoamin;

public interface CommunicationService {
	public EBikeStatesDoamin communication(EBikeStatesDoamin eBikeStates) throws InterruptedException;
}
