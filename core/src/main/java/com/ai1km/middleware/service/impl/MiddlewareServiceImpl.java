package com.ai1km.middleware.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ai1km.middleware.constant.GatewayService;
import com.ai1km.middleware.dao.TMiddlewareEBikeMacDao;
import com.ai1km.middleware.dao.TMiddlewareEBikeStatesHistoryDao;
import com.ai1km.middleware.dao.TMiddlewareEBikeTemporaryStatesDao;
import com.ai1km.middleware.domain.EBikeStatesDoamin;
import com.ai1km.middleware.entity.TMidEBikeMac;
import com.ai1km.middleware.entity.TMidEBikeStatesHistory;
import com.ai1km.middleware.entity.TMidEBikeTempStates;
import com.ai1km.middleware.service.MiddlewareService;
import com.ai1km.middleware.utils.NetworkProtocol;

import io.netty.channel.ChannelHandlerContext;

@Service
public class MiddlewareServiceImpl implements MiddlewareService{
	
	@Autowired
	TMiddlewareEBikeMacDao macDao;
	
	@Autowired
	TMiddlewareEBikeStatesHistoryDao statesHistoryDao;
	
	@Autowired
	TMiddlewareEBikeTemporaryStatesDao temporaryStatesDao;

	@Override
	public void updateMac(ChannelHandlerContext ctx,EBikeStatesDoamin eBikeStates) {
		//每次连接将其mac 和 ctx 存入map中。
		GatewayService.addGatewayChannel(eBikeStates.getMac(), ctx);
		System.out.println("--------->"+GatewayService.getGatewayChannel(eBikeStates.getMac()));
		String ipAndPort=ctx.channel().remoteAddress().toString();
		System.out.println(ipAndPort+"----客户端上线----"+eBikeStates.getMac());
		int port=Integer.valueOf(ipAndPort.split(":")[1]);
		String ip=ipAndPort.split(":")[0].split("/")[1];
		TMidEBikeMac ebikeMac=macDao.findOneByMac(eBikeStates.getMac());
		if(ebikeMac==null){
			ebikeMac=new TMidEBikeMac();
			ebikeMac.setEbikeId(000001L);
			ebikeMac.setMac(eBikeStates.getMac());
		}
		ebikeMac.setStatus(1);
		ebikeMac.setIp(ip);
		ebikeMac.setPort(port);
		macDao.save(ebikeMac);
	}

	@Override
	public void removeMac(ChannelHandlerContext ctx) {
		String ipAndPort=ctx.channel().remoteAddress().toString();
		int port=Integer.valueOf(ipAndPort.split(":")[1]);
		String ip=ipAndPort.split(":")[0].split("/")[1];
		TMidEBikeMac ebikeMac=macDao.findMacByIpAndPort(ip, port);
		System.out.println(ipAndPort+"----客户端下线----"+ebikeMac.getMac());
		ebikeMac.setStatus(2);
		macDao.save(ebikeMac);
	}

	@Override
	public void autoUpload(EBikeStatesDoamin eBikeStates) {
		System.out.println(eBikeStates);
		//更新临时状态表
		TMidEBikeTempStates ebikeTempStates=temporaryStatesDao.findOneByMac(eBikeStates.getMac());
		if(ebikeTempStates==null){
			ebikeTempStates=new TMidEBikeTempStates();
			ebikeTempStates.setEbikeId(000001L);
			ebikeTempStates.setMac(eBikeStates.getMac());
			ebikeTempStates.setStatus(1);
		}
		ebikeTempStates.setLatitude(eBikeStates.getLatitude());
		ebikeTempStates.setLongitude(eBikeStates.getLongitude());
		ebikeTempStates.setAltitude(eBikeStates.getAltitude());
		ebikeTempStates.setElectricity(eBikeStates.getElectricity());
		ebikeTempStates.setLockState(eBikeStates.getLockState());
		ebikeTempStates.setMileage(eBikeStates.getMileage());
		temporaryStatesDao.save(ebikeTempStates);
		
		//插入状态历史表
		TMidEBikeStatesHistory ebikeStatesHistory = new TMidEBikeStatesHistory();
		ebikeStatesHistory.setEbikeId(000001L);
		ebikeStatesHistory.setMac(eBikeStates.getMac());
		ebikeStatesHistory.setElectricity(eBikeStates.getElectricity());
		ebikeStatesHistory.setLatitude(eBikeStates.getLatitude());
		ebikeStatesHistory.setLongitude(eBikeStates.getLongitude());
		ebikeStatesHistory.setAltitude(eBikeStates.getAltitude());
		ebikeStatesHistory.setLockState(eBikeStates.getLockState());
		ebikeStatesHistory.setState(eBikeStates.getState());
		ebikeStatesHistory.setLight(eBikeStates.getLight());
		ebikeStatesHistory.setHorn(eBikeStates.getHorn());
		ebikeStatesHistory.setSpeed(eBikeStates.getSpeed());
		ebikeStatesHistory.setMileage(eBikeStates.getMileage());
		ebikeStatesHistory.setBump(eBikeStates.getBump());
		statesHistoryDao.save(ebikeStatesHistory);
	}

	
	
}
