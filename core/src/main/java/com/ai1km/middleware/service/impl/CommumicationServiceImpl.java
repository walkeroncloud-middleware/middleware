package com.ai1km.middleware.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ai1km.middleware.constant.GatewayService;
import com.ai1km.middleware.constant.MiddlewareConstant;
import com.ai1km.middleware.dao.TMiddlewareEbikeDataHistoryDao;
import com.ai1km.middleware.domain.EBikeStatesDoamin;
import com.ai1km.middleware.entity.TMidEbikeDataHistory;
import com.ai1km.middleware.service.CommunicationService;
import com.ai1km.middleware.utils.CommonString;
import com.ai1km.middleware.utils.NetworkProtocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

@Service
public class CommumicationServiceImpl implements CommunicationService {

	@Autowired
	TMiddlewareEbikeDataHistoryDao dataHistoryDao;
	
	private EBikeStatesDoamin returnEBikeStates;
	
	@Override
	public EBikeStatesDoamin communication(EBikeStatesDoamin eBikeStates) throws InterruptedException {
		byte[] returnData = new byte[MiddlewareConstant.RETURNDATA_LENGTH];
		TMidEbikeDataHistory ebikeDataHistory = new TMidEbikeDataHistory();
		returnEBikeStates = new EBikeStatesDoamin();
		byte[] sendData=NetworkProtocol.getSendData(eBikeStates);
		System.out.println("-----2---->"+GatewayService.getGatewayChannel(eBikeStates.getMac()));
		ChannelHandlerContext ctx=GatewayService.getGatewayChannel(eBikeStates.getMac());
		if(ctx==null){
			returnEBikeStates.setCode(1001);
			returnEBikeStates.setMessage("车辆不在线！");
			return returnEBikeStates;
		}
		ByteBuf buf = ctx.alloc().buffer();
		buf.writeBytes(sendData);
		System.out.println("-----1");
		ctx.writeAndFlush(buf);
		//首次等待接收
		synchronized(ctx){
			ctx.wait(10000);//暂时释放obj的锁，线程处于等待状态
	    }
		returnData=GatewayService.getData(eBikeStates.getMac());
		if(returnData!=null){
			System.out.println("收到");
			GatewayService.removeData(eBikeStates.getMac());
			returnEBikeStates=NetworkProtocol.getReturnStates(returnData);
			ebikeDataHistory.setReturnData(CommonString.byte2HexStr(returnData));
		}
		ebikeDataHistory.setMac(eBikeStates.getMac());
		ebikeDataHistory.setEbikeId(000001L);
		ebikeDataHistory.setFlag(eBikeStates.getReadOrWrite());
		ebikeDataHistory.setSendData(CommonString.byte2HexStr(sendData));
		if(returnEBikeStates.getMac()==null){
			returnEBikeStates.setCode(1002);
			returnEBikeStates.setMessage("数据读取失败！");
		}
		dataHistoryDao.save(ebikeDataHistory);
		return returnEBikeStates;
	}

}
