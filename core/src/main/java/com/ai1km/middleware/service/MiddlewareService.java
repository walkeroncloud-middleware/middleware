package com.ai1km.middleware.service;

import com.ai1km.middleware.domain.EBikeStatesDoamin;

import io.netty.channel.ChannelHandlerContext;

public interface MiddlewareService {
	
	public void updateMac(ChannelHandlerContext ctx,EBikeStatesDoamin eBikeStates);
	
	public void removeMac(ChannelHandlerContext ctx);
	
	public void autoUpload(EBikeStatesDoamin eBikeStates);
	
}
