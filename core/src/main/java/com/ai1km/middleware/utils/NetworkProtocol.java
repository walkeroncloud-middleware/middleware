package com.ai1km.middleware.utils;

import java.math.BigDecimal;

import com.ai1km.middleware.constant.MiddlewareConstant;
import com.ai1km.middleware.domain.EBikeStatesDoamin;

/**
 * 协议类 封装协议类
 *
 */
public class NetworkProtocol {
	
	/**
	 * 获取封装好的数据
	 * @param coreLength
	 * @return
	 */
	public static byte[] getSendData(EBikeStatesDoamin eBikeState){
		byte[] sendData = new byte[MiddlewareConstant.SENDDATA_LENGTH];
		//起始位
		sendData[0] = (byte) 0xAA;
		//标志位
		sendData[1] = (byte) 0x01;
		//数据长度
		sendData[2] = (byte) 0x02;
		String readOrWrite=eBikeState.getReadOrWrite();
		if(null!=readOrWrite){
			if(readOrWrite.equals("Read")){
				//控制位读，数据都是0
				sendData[3] = (byte) 0x03;
				sendData[4] = (byte) 0x00;
				sendData[5] = (byte) 0x00;
			}else if(readOrWrite.equals("Write")){
				//控制位写
				sendData[3] = (byte) 0x05;
				String state=eBikeState.getState();
				String lock=eBikeState.getLockState();
				String light=eBikeState.getLight();
				String horn=eBikeState.getHorn();
				byte state4Byte = (byte) 0xA0; 
				byte lock4Byte = (byte) 0x0A;
				byte light4Byte = (byte) 0xA0;
				byte horn4Byte = (byte) 0x0A;
 				if(null!=state){
					if(state.equals("ON")){
						state4Byte = (byte) 0x10;
					}else if(state.equals("OFF")){
						state4Byte = (byte) 0x00;
					}else if(state.equals("KEEP")){
						state4Byte = (byte) 0xA0;
					}
				}
				if(null!=lock){
					if(lock.equals("ON")){
						lock4Byte = (byte) 0x01;
					}else if(lock.equals("OFF")){
						lock4Byte = (byte) 0x00;
					}else if(lock.equals("KEEP")){
						lock4Byte = (byte) 0x0A;
					}
				}
				//车辆启动和上锁控制
				sendData[4]=(byte) (state4Byte+lock4Byte);
				if(null!=light){
					if(light.equals("ON")){
						light4Byte = (byte) 0x10;
					}else if(light.equals("OFF")){
						light4Byte = (byte) 0x00;
					}else if(light.equals("KEEP")){
						light4Byte = (byte) 0xA0;
					}
				}
				if(null!=horn){
					if(horn.equals("ON")){
						horn4Byte = (byte) 0x01;
					}else if(horn.equals("OFF")){
						horn4Byte = (byte) 0x00;
					}else if(horn.equals("KEEP")){
						horn4Byte = (byte) 0x0A;
					}
				}
				//车辆大灯和喇叭控制
				sendData[5]=(byte) (light4Byte+horn4Byte);
			}
			//校验位
			sendData[6] = crcCheck(sendData,6);
			sendData[7] = (byte) 0xFF;
			return sendData;
		}
		return null;
	}
	
	
	public static EBikeStatesDoamin getReturnStates(byte[] returnData){
		EBikeStatesDoamin eBikeState=new EBikeStatesDoamin();
		//起始位是）0xAA
		if(returnData[0] == (byte) 0xAA
			&&returnData[returnData.length-1]==(byte) 0xFF){
			//判断校验和
			if(returnData[returnData.length-2]==crcCheck(returnData,returnData.length-2)){
				//MAC地址
				byte[] mac4Byte= new byte[6];
				System.arraycopy(returnData, 1, mac4Byte, 0, 6);
				eBikeState.setMac(CommonString.byte2HexStr(mac4Byte));
				//标志位
				if(returnData[7]==(byte) 0x11){
					//MAC地址数据
					eBikeState.setReturnDataType(11);
				}else if(returnData[7]==(byte) 0x12){
					//车辆主动上传数据
					eBikeState.setReturnDataType(12);
				}else if(returnData[7]==(byte) 0x13){
					//要返回平台的数据
					eBikeState.setReturnDataType(13);
				}
				//获取是读或者写的返回状态
				if(returnData[9]==(byte) 0x13){
					eBikeState.setReadOrWrite("Read");
				}else if(returnData[9]==(byte) 0x15){
					eBikeState.setReadOrWrite("Write");
				}
				//获取车辆启动和上锁的状态
				if(returnData[10]==(byte) 0x00){
					eBikeState.setState("OFF");
					eBikeState.setLockState("OFF");
				}else if(returnData[10]==(byte) 0x01){
					eBikeState.setState("OFF");
					eBikeState.setLockState("ON");
				}else if(returnData[10]==(byte) 0x10){
					eBikeState.setState("ON");
					eBikeState.setLockState("OFF");
				}else if(returnData[10]==(byte) 0x11){
					eBikeState.setState("ON");
					eBikeState.setLockState("ON");
				}
				//获取车辆大灯和喇叭的状态
				if(returnData[11]==(byte) 0x00){
					eBikeState.setLight("OFF");
					eBikeState.setHorn("OFF");
				}else if(returnData[11]==(byte) 0x01){
					eBikeState.setLight("OFF");
					eBikeState.setHorn("ON");
				}else if(returnData[11]==(byte) 0x10){
					eBikeState.setLight("ON");
					eBikeState.setHorn("OFF");
				}else if(returnData[11]==(byte) 0x11){
					eBikeState.setLight("ON");
					eBikeState.setHorn("ON");
				}
				//获取车辆坐标信息
				//to returnData[25]
				//获取车辆电量
				byte[] electricity4Byte = new byte[1];
				System.arraycopy(returnData, 26, electricity4Byte, 0, 1);
				int electricity4Int=Integer.valueOf(CommonString.byte2HexStr(electricity4Byte), 16);
				BigDecimal electricity=new BigDecimal(electricity4Int);
				eBikeState.setElectricity(electricity);
				//获取车辆行驶速度
				byte[] speed4Byte = new byte[1];
				System.arraycopy(returnData, 27, speed4Byte, 0, 1);
				int speed=Integer.valueOf(CommonString.byte2HexStr(speed4Byte), 16);
				eBikeState.setSpeed(speed);
				//获取车辆行驶里程
				byte[] mileage4Byte = new byte[2];
				System.arraycopy(returnData, 28, mileage4Byte, 0, 2);
				int mileage4Int=Integer.valueOf(CommonString.byte2HexStr(mileage4Byte), 16);
				BigDecimal mileage=new BigDecimal(mileage4Int);
				eBikeState.setMileage(mileage);
				//获取车辆碰撞强度
				byte[] bump4Byte = new byte[1];
				System.arraycopy(returnData, 29, bump4Byte, 0, 1);
				int bump=Integer.valueOf(CommonString.byte2HexStr(bump4Byte), 16);
				eBikeState.setBump(bump);
				return eBikeState;
			}
		}
		eBikeState.setErrData(true);
		return eBikeState;
	}
	
	
	
	/**
	 * 指定byte数组和给定的长度length，对该byte数组的前length位做累加
	 * 
	 * @param a
	 *            byte数组
	 * @param length
	 *            指定的长度
	 * @return byte类型的结果
	 */
	private static byte crcCheck(byte[] a, int length) {
		int tmp = 0;
		int tmp1 = 0;
		for (int i = 0; i < length; i++) {
			tmp = a[i];
			tmp1 += tmp;
		}
		return (byte) (tmp1 & 0xFF);
	}

}
