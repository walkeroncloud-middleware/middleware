package com.ai1km.middleware.utils;

public class CommonString {
	public static String byte2HexStr(byte[] b) {
		StringBuilder sb=new StringBuilder();
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (Integer.toHexString(b[n] & 0XFF)).toUpperCase();
			if (stmp.length() == 1)
				sb.append("0"+ stmp);
			else
				sb.append(stmp);
		}
		return sb.toString().toUpperCase();
	}
	
	public static byte[] hexStr2Bytes(String src) throws NumberFormatException{
		int m = 0, n = 0;
		int l = src.length() / 2;
		byte[] ret = new byte[l];
		for (int i = 0; i < l; i++) {
			m = i * 2 + 1;
			n = m + 1;
			ret[i] = uniteBytes(src.substring(i * 2, m), src.substring(m, n));
		}
		return ret;
	}
	
	private static byte uniteBytes(String src0, String src1) throws NumberFormatException{
		byte b0 = Byte.decode("0x" + src0).byteValue();
		b0 = (byte) (b0 << 4);
		byte b1 = Byte.decode("0x" + src1).byteValue();
		byte ret = (byte) (b0 | b1);
		return ret;
	}
	public static String getBinaryStrFromByte(byte b) {
		String result = "";
		byte a = b;
		;
		for (int i = 0; i < 8; i++) {
			byte c = a;
			a = (byte) (a >> 1);
			a = (byte) (a << 1);
			if (a == c) {
				result = "0" + result;
			} else {
				result = "1" + result;
			}
			a = (byte) (a >> 1);
		}
		return result;
	}
}
