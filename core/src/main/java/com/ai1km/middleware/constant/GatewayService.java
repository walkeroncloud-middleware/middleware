package com.ai1km.middleware.constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.netty.channel.ChannelHandlerContext;

public class GatewayService {
    
    private static Map<String, ChannelHandlerContext> channelMap = new ConcurrentHashMap<>();
    
    private static Map<String, byte[]> dataMap = new ConcurrentHashMap<>();
    
    public static void addGatewayChannel(String id, ChannelHandlerContext gateway_channel){
    	channelMap.put(id, gateway_channel);
    }
    
    public static Map<String, ChannelHandlerContext> getChannels(){
        return channelMap;
    }

    public static ChannelHandlerContext getGatewayChannel(String id){
        return channelMap.get(id);
    }
    
    public static void removeGatewayChannel(String id){
    	channelMap.remove(id);
    }
    
    public static void addData(String id, byte[] data){
    	dataMap.put(id, data);
    }
    
    public static byte[] getData(String id){
        return dataMap.get(id);
    }
    
    public static void removeData(String id){
    	dataMap.remove(id);
    }
}