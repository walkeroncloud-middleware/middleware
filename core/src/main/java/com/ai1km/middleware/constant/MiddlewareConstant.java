package com.ai1km.middleware.constant;

public class MiddlewareConstant {
	
	public static final int SERVER_PORT_NUMBER=5600;
	
	public static final int SENDDATA_LENGTH=15;
	
	public static final int RETURNDATA_LENGTH=33;
	
	public static final long WAIT_MILLISECOND=5000;

}
