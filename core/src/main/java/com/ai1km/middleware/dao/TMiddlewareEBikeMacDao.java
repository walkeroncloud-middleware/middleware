package com.ai1km.middleware.dao;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.ai1km.middleware.entity.TMidEBikeMac;

public interface TMiddlewareEBikeMacDao extends PagingAndSortingRepository<TMidEBikeMac, Long>{
	
	//当硬件断开连接的时候 根据Ip和port 查出MAC，将他状态改变。
	@Query(value="select t from TMidEBikeMac t where t.ip=?1 and t.port=?2")
    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	TMidEBikeMac findMacByIpAndPort(String ip,int port);
	
	@Query(value="select t from TMidEBikeMac t where t.mac=?1")
    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	TMidEBikeMac findOneByMac(String mac);
	
}
