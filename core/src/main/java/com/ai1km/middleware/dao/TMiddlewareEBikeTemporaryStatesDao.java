package com.ai1km.middleware.dao;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.ai1km.middleware.entity.TMidEBikeTempStates;

public interface TMiddlewareEBikeTemporaryStatesDao extends PagingAndSortingRepository<TMidEBikeTempStates, Long>{
	
	@Query(value="select t from TMidEBikeTempStates t where t.mac=?1")
    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true") })
	TMidEBikeTempStates findOneByMac(String mac);

}
