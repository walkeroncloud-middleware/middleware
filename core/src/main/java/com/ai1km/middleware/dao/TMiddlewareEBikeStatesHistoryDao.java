package com.ai1km.middleware.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ai1km.middleware.entity.TMidEBikeStatesHistory;

public interface TMiddlewareEBikeStatesHistoryDao extends PagingAndSortingRepository<TMidEBikeStatesHistory, Long>{

}
