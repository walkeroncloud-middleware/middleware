package com.ai1km.middleware.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ai1km.middleware.entity.TMidEbikeDataHistory;

public interface TMiddlewareEbikeDataHistoryDao extends PagingAndSortingRepository<TMidEbikeDataHistory, Long>{

}
