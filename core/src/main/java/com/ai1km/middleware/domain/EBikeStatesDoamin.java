package com.ai1km.middleware.domain;

import java.math.BigDecimal;

public class EBikeStatesDoamin extends ErrorData{
	
	//车辆id
	private Long ebikeId;
	
	//车辆MAC地址 
	private String mac;
	
	//车辆电量
	private BigDecimal electricity;
	
	//车辆坐标纬度
	private BigDecimal latitude;
	
	//车辆坐标经度
	private BigDecimal longitude;
	
	//车辆坐标海拔
	private BigDecimal altitude;
	
	//车辆锁
	private String lockState;
	
	//车辆发动状态
	private String state;
	
	//车辆大灯
	private String light;
	
	//车辆喇叭
	private String horn;
	
	//车辆行驶速度
	private int speed;
	
	//车辆行驶里程
	private BigDecimal mileage;
	
	//车辆碰撞指数
	private int bump;
	
	//读或写
    private String readOrWrite;
    
    //返回数据类型
    private int returnDataType;
    
    //错误数据
    private boolean errData;
	
    
	public Long getEbikeId() {
		return ebikeId;
	}

	public void setEbikeId(Long ebikeId) {
		this.ebikeId = ebikeId;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public BigDecimal getElectricity() {
		return electricity;
	}

	public void setElectricity(BigDecimal electricity) {
		this.electricity = electricity;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getAltitude() {
		return altitude;
	}

	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

	public String getLockState() {
		return lockState;
	}

	public void setLockState(String lockState) {
		this.lockState = lockState;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLight() {
		return light;
	}

	public void setLight(String light) {
		this.light = light;
	}

	public String getHorn() {
		return horn;
	}

	public void setHorn(String horn) {
		this.horn = horn;
	}
	
	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public BigDecimal getMileage() {
		return mileage;
	}

	public void setMileage(BigDecimal mileage) {
		this.mileage = mileage;
	}

	public int getBump() {
		return bump;
	}

	public void setBump(int bump) {
		this.bump = bump;
	}

	public String getReadOrWrite() {
		return readOrWrite;
	}

	public void setReadOrWrite(String readOrWrite) {
		this.readOrWrite = readOrWrite;
	}
	
	public int getReturnDataType() {
		return returnDataType;
	}

	public void setReturnDataType(int returnDataType) {
		this.returnDataType = returnDataType;
	}

	
	public boolean getErrData() {
		return errData;
	}

	public void setErrData(boolean errData) {
		this.errData = errData;
	}

	@Override
	public String toString() {
		return "EBikeStatesDoamin [ebikeId=" + ebikeId + ", mac=" + mac + ", electricity=" + electricity + ", latitude="
				+ latitude + ", longitude=" + longitude + ", altitude=" + altitude + ", lockState=" + lockState
				+ ", state=" + state + ", light=" + light + ", horn=" + horn + ", speed=" + speed + ", mileage="
				+ mileage + ", bump=" + bump + ", readOrWrite=" + readOrWrite + ", returnDataType=" + returnDataType
				+ "]";
	}
	

}
