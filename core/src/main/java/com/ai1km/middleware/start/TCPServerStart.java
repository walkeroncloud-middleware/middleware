package com.ai1km.middleware.start;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ai1km.middleware.constant.MiddlewareConstant;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

@Service("tcpServer")
public class TCPServerStart {
	
	public void serverStart() throws Exception {
		Thread server = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					initServer();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		server.start();
	}
	
	public void initServer() throws Exception {
		System.out.println("--------启动线程----------");
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup);
			b.channel(NioServerSocketChannel.class);
			b.childHandler(new MiddlewareInitializer());
			// 服务器绑定端口监听
			ChannelFuture f = b.bind(MiddlewareConstant.SERVER_PORT_NUMBER).sync();
			// 监听服务器关闭监听
			f.channel().closeFuture().sync();
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}
}
