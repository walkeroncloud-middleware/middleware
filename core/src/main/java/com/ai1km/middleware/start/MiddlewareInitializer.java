package com.ai1km.middleware.start;

import org.springframework.stereotype.Service;

import com.ai1km.middleware.utils.SpringUtil;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.bytes.ByteArrayDecoder;



@Service("middlewareInitializer")
public class MiddlewareInitializer extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();

		//监测客户端断开
		pipeline.addLast("handleRemoved", getMiddlewareHandleRemoved());
		// 字符串解码 和 编码
		//每次数据都要以0xff结尾。做分包
		//byte[] endData=new byte[1];
		//endData[0]=(byte) 0xff;
		//ByteBuf endData4ByteBuf= Unpooled.copiedBuffer(endData);
		//ByteBuf endData4ByteBuf= Unpooled.copiedBuffer("##".getBytes());
		//pipeline.addLast(new DelimiterBasedFrameDecoder(1024,endData4ByteBuf));
		pipeline.addLast(new FixedLengthFrameDecoder(33));
		pipeline.addLast("decoder", new ByteArrayDecoder());
		pipeline.addLast("encoder", new ByteArrayDecoder());
		// 自己的逻辑Handler
		pipeline.addLast("handler", getMiddlewareHandler());
	}
	
	private MiddlewareHandler getMiddlewareHandler(){
		return (MiddlewareHandler)SpringUtil.getObject("middlewareHandler");
	}
	
	private MiddlewareHandleRemoved getMiddlewareHandleRemoved(){
		return (MiddlewareHandleRemoved)SpringUtil.getObject("middlewareHandleRemoved");
	}
	
	
}
