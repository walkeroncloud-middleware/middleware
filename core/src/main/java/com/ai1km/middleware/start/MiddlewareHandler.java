package com.ai1km.middleware.start;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ai1km.middleware.constant.GatewayService;
import com.ai1km.middleware.constant.MiddlewareConstant;
import com.ai1km.middleware.domain.EBikeStatesDoamin;
import com.ai1km.middleware.service.MiddlewareService;
import com.ai1km.middleware.utils.CommonString;
import com.ai1km.middleware.utils.NetworkProtocol;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

@Service("middlewareHandler")
public class MiddlewareHandler extends SimpleChannelInboundHandler<byte[]>{
	
	@Autowired
	MiddlewareService middlewareService;
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, byte[] msg) throws Exception {
		EBikeStatesDoamin returnStates= NetworkProtocol.getReturnStates(msg);
		System.out.println(returnStates+"==="+CommonString.byte2HexStr(msg));
		if(returnStates!=null){
			if(returnStates.getErrData()){
				ctx.channel().close();
			}else if(returnStates.getReturnDataType()==11){
				middlewareService.updateMac(ctx,returnStates);
			}else if(returnStates.getReturnDataType()==12){
				middlewareService.autoUpload(returnStates);
			}else if(returnStates.getReturnDataType()==13){
				GatewayService.addData(returnStates.getMac(), msg);
				synchronized(ctx){
					ctx.notify();//唤醒对obj加锁的线程
			    }
			}
		}
	}
	/*
	 * 
	 * 覆盖 channelActive 方法 在channel被启用的时候触发 (在建立连接的时候)
	 * 
	 * channelActive 和 channelInActive 在后面的内容中讲述，这里先不做详细的描述
	 * */
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		
//		byte[] msg=new byte[1];
//		msg[0]=(byte) 0xff;
//		ByteBuf buf = ctx.alloc().buffer();
//		System.out.println(msg.length);
//		buf.writeInt(msg.length);
//		buf.writeBytes(msg);
//		ctx.writeAndFlush(buf);
//		System.out.println("-------->");
//		GatewayService.addGatewayChannel("hello", ctx);
		super.channelActive(ctx);
	}
	
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		// TODO Auto-generated method stub
		super.exceptionCaught(ctx, cause);
	}
	
}
