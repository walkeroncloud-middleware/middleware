package com.ai1km.middleware.start;

import org.springframework.beans.factory.annotation.Autowired;

import com.ai1km.middleware.service.MiddlewareService;

import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

public class MiddlewareHandleRemoved extends ChannelHandlerAdapter {

	@Autowired
	MiddlewareService middlewareService;
	
	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		middlewareService.removeMac(ctx);
		super.handlerRemoved(ctx);
	}

}