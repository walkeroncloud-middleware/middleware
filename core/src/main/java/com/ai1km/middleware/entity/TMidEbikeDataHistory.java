package com.ai1km.middleware.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_MID_EBiKE_DATA_HISTORY")
public class TMidEbikeDataHistory extends BaseEntity{
	
	@Id
	@GeneratedValue
	@Column(name = "data_id")
	private long dataId;
	
	@Column(name = "ebike_id")
	private Long ebikeId;
	
	@Column(name = "flag")
	private String flag;
	
	@Column(name = "mac")
	private String mac;
	
	@Column(name = "send_data")
	private String sendData; 
	
	@Column(name = "return_data")
	private String returnData;

	public long getDataId() {
		return dataId;
	}

	public void setDataId(long dataId) {
		this.dataId = dataId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getSendData() {
		return sendData;
	}

	public void setSendData(String sendData) {
		this.sendData = sendData;
	}

	public String getReturnData() {
		return returnData;
	}

	public void setReturnData(String returnData) {
		this.returnData = returnData;
	}

	public Long getEbikeId() {
		return ebikeId;
	}

	public void setEbikeId(Long ebikeId) {
		this.ebikeId = ebikeId;
	} 
	
	
	
	
}
