package com.ai1km.middleware.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * Created by justin on 16-9-10.
 */
@MappedSuperclass
public abstract class BaseEntity {
	@Column(name = "insert_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date insertTime;
	@Column(name = "update_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@PrePersist
	protected void prepersist() {
		Date currentDate = new Date();
		this.insertTime = currentDate;
		this.updateTime = currentDate;
	}

	@PreUpdate
	protected void preUpdate() {
		Date currentDate = new Date();
		
		if (this.insertTime == null)
		{
			this.insertTime = currentDate;
		}
		this.updateTime = currentDate;
	}
}
