package com.ai1km.middleware.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_MID_EBiKE_MAC")
public class TMidEBikeMac extends BaseEntity{
	
	@Id
	@GeneratedValue
	@Column(name = "mac_id")
	private Long macId;
	
	@Column(name = "ebike_id")
	private Long ebikeId;
	
	@Column(name = "mac")
	private String mac;
	
	@Column(name = "ip")
	private String ip;
	
	@Column(name="port")
	private int port;
	
	@Column(name="status")
	private int status;

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Long getMacId() {
		return macId;
	}

	public void setMacId(Long macId) {
		this.macId = macId;
	}

	public Long getEbikeId() {
		return ebikeId;
	}

	public void setEbikeId(Long ebikeId) {
		this.ebikeId = ebikeId;
	}
	
	
	
	
	
	
}
