package com.ai1km.middleware.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_MID_EBiKE_STATES_HISTORY")
public class TMidEBikeStatesHistory extends BaseEntity{
	
	@Id
	@GeneratedValue
	@Column(name = "state_id")
	private Long stateId; 
	
	@Column(name = "ebike_id")
	private Long ebikeId;
	
	@Column(name = "mac")
	private String mac;
	
	@Column(name = "electricity")
	private BigDecimal electricity;
	
	@Column(name = "latitude")
	private BigDecimal latitude;
	
	@Column(name = "longitude")
	private BigDecimal longitude;
	
	@Column(name = "altitude")
	private BigDecimal altitude;
	
	@Column(name = "lock_state")
	private String lockState;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "light")
	private String light;
	
	@Column(name = "horn")
	private String horn;
	
	@Column(name = "speed")
	private int speed;
	
	@Column(name = "mileage")
	private BigDecimal mileage;
	
	@Column(name = "bump")
	private int bump;

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public BigDecimal getElectricity() {
		return electricity;
	}

	public void setElectricity(BigDecimal electricity) {
		this.electricity = electricity;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getAltitude() {
		return altitude;
	}

	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

	public String getLockState() {
		return lockState;
	}

	public void setLockState(String lockState) {
		this.lockState = lockState;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLight() {
		return light;
	}

	public void setLight(String light) {
		this.light = light;
	}

	public String getHorn() {
		return horn;
	}

	public void setHorn(String horn) {
		this.horn = horn;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public BigDecimal getMileage() {
		return mileage;
	}

	public void setMileage(BigDecimal mileage) {
		this.mileage = mileage;
	}

	public int getBump() {
		return bump;
	}

	public void setBump(int bump) {
		this.bump = bump;
	}

	public Long getEbikeId() {
		return ebikeId;
	}

	public void setEbikeId(Long ebikeId) {
		this.ebikeId = ebikeId;
	}
	
	
}
