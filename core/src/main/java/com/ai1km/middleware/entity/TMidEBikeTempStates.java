package com.ai1km.middleware.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_MID_EBiKE_TEMP_STATES")
public class TMidEBikeTempStates extends BaseEntity{

	@Id
	@GeneratedValue
	@Column(name = "temp_state_id")
	private Long tempStateId;
	
	@Column(name = "ebike_id")
	private Long ebikeId;
	
	@Column(name = "mac")
	private String mac;
	
	@Column(name = "status")
	private int status;
	
	@Column(name = "latitude")
	private BigDecimal latitude;
	
	@Column(name = "longitude")
	private BigDecimal longitude;
	
	@Column(name = "altitude")
	private BigDecimal altitude;
	
	@Column(name = "electricity")
	private BigDecimal electricity;
	
	@Column(name = "lock_state")
	private String lockState;
	
	@Column(name = "mileage")
	private BigDecimal mileage;
	

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getAltitude() {
		return altitude;
	}

	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

	public BigDecimal getElectricity() {
		return electricity;
	}

	public void setElectricity(BigDecimal electricity) {
		this.electricity = electricity;
	}

	public String getLockState() {
		return lockState;
	}

	public void setLockState(String lockState) {
		this.lockState = lockState;
	}

	public BigDecimal getMileage() {
		return mileage;
	}

	public void setMileage(BigDecimal mileage) {
		this.mileage = mileage;
	}

	public Long getTempStateId() {
		return tempStateId;
	}

	public void setTempStateId(Long tempStateId) {
		this.tempStateId = tempStateId;
	}

	public Long getEbikeId() {
		return ebikeId;
	}

	public void setEbikeId(Long ebikeId) {
		this.ebikeId = ebikeId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	

}
