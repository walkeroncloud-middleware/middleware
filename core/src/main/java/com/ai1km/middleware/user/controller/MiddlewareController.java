package com.ai1km.middleware.user.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ai1km.middleware.domain.EBikeStatesDoamin;
import com.ai1km.middleware.service.CommunicationService;

@Controller
@RequestMapping(value = "/controller")
public class MiddlewareController {
	
	@Autowired
	CommunicationService communicationService;
	
	@RequestMapping(value = "/communication", method = POST)
	@ResponseBody
	public EBikeStatesDoamin communication(@RequestBody EBikeStatesDoamin eBikeStates) throws InterruptedException{
		System.out.println("===asd");
		System.out.println("--------->"+eBikeStates.getMac());
		return communicationService.communication(eBikeStates);
	}
}
