package com.ai1km.middleware.user.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ai1km.middleware.constant.GatewayService;
import com.ai1km.middleware.service.MiddlewareService;
import com.ai1km.middleware.utils.CommonString;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

@Controller
@RequestMapping(value = "/test")
public class TestController {

	@Autowired
	MiddlewareService middlewareService;
	
	@RequestMapping(value = "/sayHello", method = GET)
	@ResponseBody
	public String sayHello() throws InterruptedException {
		System.out.println("shoudao");
		ChannelHandlerContext ctx=GatewayService.getGatewayChannel("hello");
		byte[] msg=new byte[1];
		msg[0]=(byte) 0x81;
		ByteBuf buf = ctx.alloc().buffer();
		System.out.println(msg.length);
		buf.writeInt(msg.length);
		buf.writeBytes(msg);
		ctx.writeAndFlush(buf);
		String ss=null;
		while(true){
			byte[] msg1=GatewayService.getData("return");
			if(msg1!=null){
				System.out.println("fafffff");
				ss=CommonString.byte2HexStr(msg1);
				GatewayService.removeData("return");
				return ss;
			}
			Thread.sleep(100);
		}
	}
	
	public static void main(String args[]){
		byte[] speed4Byte = new byte[1];
		//System.arraycopy(returnData, 26, speed4Byte, 0, 1);
		speed4Byte[0] = (byte) 0xAA;
		int speed=Integer.valueOf(CommonString.byte2HexStr(speed4Byte), 16);
		System.out.println(speed);
	}
	

}
