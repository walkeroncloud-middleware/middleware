DROP TABLE IF EXISTS T_MID_EBiKE_STATES_HISTORY;  
CREATE TABLE T_MID_EBiKE_STATES_HISTORY(  
  state_id integer NOT NULL AUTO_INCREMENT,  
  ebike_id integer NOT NULL,  
  mac varchar(20) NOT NULL,
  state varchar(5),
  latitude decimal default 0,
  longitude decimal default 0,
  altitude decimal default 0,
  electricity decimal default 0,
  lock_state varchar(5),
  mileage decimal default 0,
  light varchar(5),
  horn varchar(5),
  speed integer,
  bump integer,
  insert_time  datetime,
  update_time datetime,
  PRIMARY KEY (state_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
