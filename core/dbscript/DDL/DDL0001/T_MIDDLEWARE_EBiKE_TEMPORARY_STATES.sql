DROP TABLE IF EXISTS T_MID_EBiKE_TEMPORARY_STATES;  
CREATE TABLE T_MID_EBiKE_TEMPORARY_STATES(  
  temp_state_id integer NOT NULL AUTO_INCREMENT,  
  ebike_id integer NOT NULL,  
  mac varchar(20) NOT NULL,
  status integer,
  latitude decimal default 0,
  longitude decimal default 0,
  altitude decimal default 0,
  electricity decimal default 0,
  lock_state varchar(5),
  mileage decimal default 0,
  insert_time  datetime,
  update_time datetime,
  PRIMARY KEY (temporary_state_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
