DROP TABLE IF EXISTS T_MID_EBiKE_DATA_HISTORY;  
CREATE TABLE T_MID_EBiKE_DATA_HISTORY (  
  data_id integer NOT NULL AUTO_INCREMENT,  
  ebike_id integer NOT NULL,  
  mac varchar(20) NOT NULL,
  flag varchar(10),
  send_data varchar(255),
  return_data varchar(255),
  insert_time  datetime,
  update_time datetime,
  PRIMARY KEY (data_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
