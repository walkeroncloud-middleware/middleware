DROP TABLE IF EXISTS T_MID_EBiKE_MAC;  
CREATE TABLE T_MID_EBiKE_MAC (  
  mac_id integer NOT NULL AUTO_INCREMENT,  
  ebike_id integer NOT NULL,  
  mac varchar(20) NOT NULL,
  ip  varchar(20),
  port integer,
  status integer,
  insert_time  datetime  ,
  update_time datetime  ,
  PRIMARY KEY (mac_id)  
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
